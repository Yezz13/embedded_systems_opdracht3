fetch('http://rpi-arthur/data.json')
  .then(function(response) {
    return response.json();
  })
  .then(function(data) {
		
		for (var i = 0; i < data.length; i++) {
			var persoon = data[i];
			var element = document.createElement("div");
			var elementid = document.createElement("div");
			var elementtijd = document.createElement("div");
			var elementvoornaam = document.createElement("div");
			var elementachternaam = document.createElement("div");
			var elementleeftijd = document.createElement("div");

			element.setAttribute("class", "row");
			element.setAttribute("id", "rij");
			elementid.setAttribute("class", "col-sm-2");
			elementtijd.setAttribute("class", "col-sm-4");
			elementvoornaam.setAttribute("class", "col-sm-2");
			elementachternaam.setAttribute("class", "col-sm-2");
			elementleeftijd.setAttribute("class", "col-sm-2");

			elementid.textContent = persoon.id;
			elementtijd.textContent = persoon.tijd;
			elementvoornaam.textContent = persoon.voornaam;
			elementachternaam.textContent = persoon.achternaam;
			elementleeftijd.textContent = persoon.leeftijd;

			document.getElementById("tabel").appendChild(element);
			document.getElementById("rij").appendChild(elementid);
			document.getElementById("rij").appendChild(elementtijd);
			document.getElementById("rij").appendChild(elementvoornaam);
			document.getElementById("rij").appendChild(elementachternaam);
			document.getElementById("rij").appendChild(elementleeftijd);
	}
  });
